
create table patient(
    Customer_name varchar(255),
    Customer_ID varchar(18),
    Customer_Open_Date date,
    Last_Consulted_Datev date,
    Vaccinantion_Type char(5),
    Doctor_Consulted char(255),
    State char(5),
    Country char(5),
    Date_of_Birth date,
    Active_Customer char(1)
)
row format delimited
fields terminated by '|'
stored as textfile
TBLPROPERTIES('skip.header.line.count'='1');


load data local inpath '/home/hrushi/Desktop/assi1'
into table patient;

output :

+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+
| patient.customer_name  | patient.customer_id  | patient.customer_open_date  | patient.last_consulted_datev  | patient.vaccinantion_type  |              patient.doctor_consulted              | patient.state  | patient.country  | patient.date_of_birth  | patient.active_customer  |
+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+
| Alex                   | 123457               | NULL                        | NULL                          | MVD                        | Paul                                                                                                                                                                                                                                                            | SA             | USA              | NULL                   | A                        |
| John                   | 123458               | NULL                        | NULL                          | MVD                        | Paul                                                                                                                                                                                                                                                            | TN             | IND              | NULL                   | A                        |
| Mathew                 | 123459               | NULL                        | NULL                          | MVD                        | Paul                                                                                                                                                                                                                                                            | WAS            | PHIL             | NULL                   | A                        |
| Matt                   | 12345                | NULL                        | NULL                          | MVD                        | Paul                                                                                                                                                                                                                                                            | BOS            | NYC              | NULL                   | A                        |
| Jacob                  | 1256                 | NULL                        | NULL                          | MVD                        | Paul                                                                                                                                                                                                                                                            | VIC            | AU               | NULL                   | A                        |
+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+



alter table patient
change customer_open_date customer_open_date string ;

alter table patient
change last_consulted_datev last_consulted_date string ;

alter table patient
change date_of_birth date_of_birth string ;

load data local inpath '/home/hrushi/Desktop/assi1'
into table patient;

"""getting error value on date """

drop table patient ;

create table patient(
    Customer_name varchar(255),
    Customer_ID varchar(18),
    Customer_Open_Date date,
    Last_Consulted_Datev date,
    Vaccinantion_Type char(5),
    Doctor_Consulted char(255),
    State char(5),
    Country char(5),
    Date_of_Birth date,
    Active_Customer char(1)
)
row format delimited
fields terminated by ','
stored as textfile
TBLPROPERTIES('skip.header.line.count'='1');

""" Doing some changes in formatting of date and creating 21 csv file named assi11.csv """



load data local inpath '/home/hrushi/Desktop/assi11.csv'
into table patient;

select * from patient ;

+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+
| patient.customer_name  | patient.customer_id  | patient.customer_open_date  | patient.last_consulted_datev  | patient.vaccinantion_type  |              patient.doctor_consulted              | patient.state  | patient.country  | patient.date_of_birth  | patient.active_customer  |
+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+
| Alex                   | 123457               | 2010-10-12                  | 2012-10-13                    | MVD                        | Paul                                                                                                                                                                                                                                                            | SA             | USA              | 1987-06-03             | A                        |
| John                   | 123458               | 2010-10-12                  | 2012-10-13                    | MVD                        | Paul                                                                                                                                                                                                                                                            | TN             | IND              | 1987-06-03             | A                        |
| Mathew                 | 123459               | 2010-10-12                  | 2012-10-13                    | MVD                        | Paul                                                                                                                                                                                                                                                            | WAS            | PHIL             | 1987-06-03             | A                        |
| Matt                   | 12345                | 2010-10-12                  | 2012-10-13                    | MVD                        | Paul                                                                                                                                                                                                                                                            | BOS            | NYC              | 1987-06-03             | A                        |
| Jacob                  | 1256                 | 2010-10-12                  | 2012-10-13                    | MVD                        | Paul                                                                                                                                                                                                                                                            | VIC            | AU               | 1987-06-03             | A                        |
+------------------------+----------------------+-----------------------------+-------------------------------+----------------------------+----------------------------------------------------+----------------+------------------+------------------------+--------------------------+


select country from patient where group by "Country" ;

to get details of indian patient

select * from patient where country = 'IND';



"""also if we want data in country-wise table we can do partitioning """



create table patient_country_wise(
    Customer_name varchar(255),
    Customer_ID varchar(18),
    Customer_Open_Date date,
    Last_Consulted_Datev date,
    Vaccinantion_Type char(5),
    Doctor_Consulted char(255),
    State char(5),
    Date_of_Birth date,
    Active_Customer char(1)
)
PARTITIONED BY (Country char(5))
row format delimited
fields terminated by ','
stored as textfile;
TBLPROPERTIES('skip.header.line.count'='1');


load data local inpath '/home/hrushi/Desktop/assi11.csv'
into table patient_country_wise;


